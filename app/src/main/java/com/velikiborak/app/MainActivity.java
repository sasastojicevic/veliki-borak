package com.velikiborak.app;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.firebase.messaging.FirebaseMessaging;
import com.nipunbirla.boxloader.BoxLoaderView;

import androidx.appcompat.app.AppCompatActivity;
//mogucnosti sa novije na starije platforme.

public class MainActivity extends AppCompatActivity {
    private WebView webView;
    private BoxLoaderView boxLoaderView;


    @Override //Kreiranje glavne aktivnosti
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = findViewById(R.id.web_view);
        boxLoaderView = findViewById(R.id.progress);

        webView.setVisibility(View.INVISIBLE);
        webView.loadUrl("https://velikiborak.com/");
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                webView.setVisibility(View.VISIBLE);
                boxLoaderView.setVisibility(View.GONE);
            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic("all"); //Registracija TOPIC-a na FCM-u


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Provera aktivnosti da li postoji akcija za dugme nazad i istorija ucitanih strana
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        // Ukoliko ne postoji istorija strana ili akcija dugmeta za nazad > izlaz iz aplikacije
        return super.onKeyDown(keyCode, event);
    }


}
